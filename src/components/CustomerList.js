import React from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import Axios from 'axios';


class CustomerListPage extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: false,
      pages: 0,
      search: ''
    };
  }


  render() {
    const { data } = this.state;
    return (
      <React.Fragment>

        <span className="waves-effect waves-light btn" style={{ cursor: 'pointer' }}
          onClick={() => {
            this.props.history.push('/customers/add');
          }}>
          New Customer
            </span>

        <ReactTable
          data={data}
          pages={this.state.pages}
          columns={[
            {
              Header: "Index",
              accessor: "id",
              show: false
            },
            {
              Header: "Status",
              accessor: "first_name"
            },
            {
              Header: "Name",
              accessor: "last_name"
            },
            {
              Header: "E-mail",
              accessor: "email"
            },
            {
              Header: "Gender",
              accessor: "gender"
            },
            {
              Header: "Phone",
              accessor: "phone"
            },
            {
              Header: "Edit",
              id: 'Edit',
              accessor: str => "Edit",

              Cell: (row) => (
                <span style={{ cursor: 'pointer', color: 'blue', textDecoration: 'underline' }}
                  onClick={() => {
                    this.props.history.push('/customers/' + this.state.data[row.index].id);
                  }}>
                  Düzenle
            </span>
              )
            }
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
          loading={this.state.loading}
          showPagination={true}
          showPaginationTop={false}
          showPaginationBottom={true}
          pageSizeOptions={[5, 10, 20, 25, 50, 100]}
          manual // this would indicate that server side pagination has been enabled 
          onFetchData={(state, instance) => {
            this.setState({ loading: true });
            this.getTestData(state.page, state.pageSize, state.sorted, state.filtered, (res) => {
              this.setState({
                data: res.data[0],
                pages: Math.ceil(res.data[1] / state.pageSize),
                loading: false
              })
            });
          }}
        />

      </React.Fragment>

    );
  }
  getTestData(page, pageSize, sorted, filtered, handleRetrievedData) {
    let url = this.baseURL + "/getData";
    let postObject = {
      page: page,
      pageSize: pageSize,
      sorted: sorted,
      filtered: filtered,
    };

    return this.post(url, postObject).then(response => handleRetrievedData(response)).catch(response => console.log(response));
  }

  async post(url, params = {}) {
    let data = await Axios.get(`http://localhost:3010/customers?page=${params.page}&pageSize=${params.pageSize}`)
    return data;
  }
}
export default CustomerListPage;