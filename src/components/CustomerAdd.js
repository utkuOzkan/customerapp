import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import Select from 'react-select';

const options = [
    { value: 'MALE', label: 'Male' },
    { value: 'FEMALE', label: 'Female' }
];

class CustomerAdd extends Component {

    state = {
        selectedOption:
            { value: 'MALE', label: 'Male' }
        ,
    };

    handleChange = selectedOption => {
        this.setState({ selectedOption });
    };

    addCustomer(newMeetup) {
        Axios.request({
            method: 'post',
            url: 'http://localhost:3010/customers',
            data: newMeetup
        }).then(response => {
            this.props.history.push('/');
        }).catch(err => console.log(err));
    }


    onSubmit(e) {
        const newMeetup = {
            fName: this.refs.fName.value,
            lName: this.refs.lName.value,
            email: this.refs.email.value,
            gender: this.state.selectedOption.value,
            phone: this.refs.phone.value
        }
        this.addCustomer(newMeetup);
        e.preventDefault();
    }

    render() {
        const { selectedOption } = this.state;

        return (
            <div>
                <br />
                <Link className="btn grey" to="/">Back</Link>
                <h1>Add Customer</h1>

                <form onSubmit={this.onSubmit.bind(this)}>
                    <div className="input-field">
                        <input type="text" name="fName" ref="fName" />
                        <label htmlFor="fName">First Name</label>
                    </div>
                    <div className="input-field">
                        <input type="text" name="lName" ref="lName" />
                        <label htmlFor="lName">Last Name</label>
                    </div>
                    <div className="input-field">
                        <input type="text" name="email" ref="email" />
                        <label htmlFor="email">E-Mail</label>
                    </div>

                    <div className="col-md-4">
                        {/* <input type="text" name="gender" ref="gender" />
                        <label htmlFor="gender">Gender</label> */}
                        <Select value={selectedOption} onChange={this.handleChange} options={options}
                        />
                    </div>

                    <div className="input-field">
                        <input type="text" name="phone" ref="phone" />
                        <label htmlFor="phone">Phone</label>
                    </div>

                    <input type="submit" value="Save" className="btn" />
                </form>


            </div>
        )

    }
}

export default CustomerAdd;