import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

class CustomerEdit extends Component{
  constructor(props){
    super(props);
    this.state = {
        fName:'',
        lName:'',
        email:'',
        gender:'',
        phone:''

    }

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentWillMount(){
    this.getCustomerDetails();
  }

  getCustomerDetails(){
    let customerId = this.props.match.params.id;
    Axios.get(`http://localhost:3010/customers/${customerId}`)
    .then(response => {
      this.setState({
        id: response.data.id,
        fName: response.data.first_name,
        lName: response.data.last_name,
        email: response.data.email,
        gender: response.data.gender,
        phone: response.data.phone
      }, () => {
      });
    })
    .catch(err => console.log(err));
    }

  editCustomer(newCustomer){
      console.log(this.state.id);
    Axios.request({
      method:'put',
      url:`http://localhost:3010/customers/${this.state.id}`,
      data: newCustomer
    }).then(response => {
      this.props.history.push('/');
    }).catch(err => console.log(err));
  }

  onSubmit(e){
    e.preventDefault();
    const newCustomer = {
        fName: this.refs.fName.value,
        lName: this.refs.lName.value,
        email: this.refs.email.value,
        gender: this.refs.gender.value,
        phone: this.refs.phone.value,
    }
    // console.log(newCustomer);
    this.editCustomer(newCustomer);
 
  }

  handleInputChange(e){
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render(){
    return (
     <div>
        <br />
       <Link className="btn grey" to={'/customers/'+this.props.match.params.id}>Back</Link>
       <h1>Edit Customer</h1>
       <form onSubmit={this.onSubmit.bind(this)}>
          <div className="input-field">
            <input type="text" name="fName" ref="fName" value={this.state.fName}  onChange={this.handleInputChange} />
            <label  className="active" htmlFor="fName">Name</label>
          </div>

          <div className="input-field">
            <input type="text" name="lName" ref="lName" value={this.state.lName}  onChange={this.handleInputChange} />
            <label  className="active" htmlFor="lName">Last Name</label>
          </div>

          <div className="input-field">
            <input type="text" name="email" ref="email" value={this.state.email}  onChange={this.handleInputChange} />
            <label  className="active" htmlFor="email">E-Mail</label>
          </div>

          <div className="input-field">
            <input type="text" name="gender" ref="gender" value={this.state.gender}  onChange={this.handleInputChange} />
            <label  className="active" htmlFor="gender">Gender</label>
          </div>
          

          <div className="input-field">
            <input type="text" name="phone" ref="phone" value={this.state.phone}  onChange={this.handleInputChange} />
            <label className="active" htmlFor="phone">Phone</label>
          </div>
          

          <input type="submit" value="Save" className="btn" />
        </form>
      </div>
    )
  }
}

export default CustomerEdit;