
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';


const linkStyle = {
    marginRight: '10px',
   
  };

class CustomerDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            details: ''
        }
    }

    componentWillMount() {
        this.getCustomer();
    }

    getCustomer() {
        let customerId = this.props.match.params.id;
        axios.get(`http://localhost:3010/customers/${customerId}`)
            .then(response => {
                this.setState({ details: response.data }, () => {
                    // console.log(this.state);
                })
            })
            .catch(err => console.log(err));
    }

    onDelete() {
        let customerId = this.state.details.id;
        axios.delete(`http://localhost:3010/customers/${customerId}`)
            .then(response => {
                this.props.history.push('/');
            }).catch(err => console.log(err));
    }

    render() {
        return (
            <div>
                <br />
                <Link className="btn grey" to='/'>Back</Link>

                <ul className="collection">
                    <li className="collection-item">Name: {this.state.details.first_name}</li>
                    <li className="collection-item">Last Name: {this.state.details.last_name}</li>
                    <li className="collection-item">E-Mail: {this.state.details.email}</li>
                    <li className="collection-item">Gender: {this.state.details.gender}</li>
                    <li className="collection-item">Phone: {this.state.details.phone}</li>
                </ul>

                <button onClick={this.onDelete.bind(this)} className="btn red right">Delete</button>
                <Link className="btn green right"  style={linkStyle} to={`/customers/edit/${this.state.details.id}` }> Edit</Link>

            </div>
        )
    }
}

export default CustomerDetail;
