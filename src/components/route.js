import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CustomerAdd from './CustomerAdd';
import CustomerListPage from './CustomerList';
import CustomerDetail from './CustomerDetail';
import CustomerEdit from './CustomerEdit';
 
 

const Main = () => (
    <main>
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={CustomerListPage} />
                <Route exact path='/customers/add' component={CustomerAdd} />
                <Route exact path='/customers/:id' component={CustomerDetail}></Route>
                <Route exact path='/customers/edit/:id' component={CustomerEdit} />
            </Switch>
        </BrowserRouter>
    </main>
)

export default Main;